@extends('layouts.app')

@section('page-title', 'Blog List')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-1">
                <a href="{{ route('news.index') }}">Home</a>
            </div>
            <div class="col-1">
                <a href="{{ route('news.create') }}">Add Form</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                {!! '<h4 class="text-center">Blog Listing </h4>' !!}
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <table class="table table-sm table-hover table-border">
                    <thead class="thead-dark">
                    <th>S.N</th>
                    <th>Title</th>
                    <th>Summary</th>
                    <th>Status</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @if(isset($data))
                        @foreach($data as $key=>$value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value['title'] }}</td>
                                <td>{{ $value['summary'] }}</td>
                                <td>{{ $value['status'] }}</td>
                                <td>{{ $value['category'] }}</td>
                                <td>
                                <img src="{{ asset('image/'.$value['image']) }}" style="width: 200px; " />
                                </td>
                                <td>Edit|Delete</td>
                            </tr>

                            @endforeach

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection
