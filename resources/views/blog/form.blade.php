@extends('layouts.app')

@section('page-title', 'Blog Form')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="{{ route('news.index') }}">Home</a>
            </div>
            <div class="col-1">
                <a href="{{ route('news.create') }}">Add Form</a>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    {!! '<h4 class="text-center">News Add </h4>' !!}
                </div>
            </div>

            <div class="row">
                <div class="col-12">

                    <form action="{{ route('news.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        {{-- Method spoofing(it is a method to change the method ) --}}
                        {{--<input type="hidden" name="method" value="patch">--}}
                        {{--@method('patch/put/')--}}

                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Title:</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" value="{{ old('title') }}" class="form-control form-control-sm">
                                @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Summary:</label>
                            <div class="col-sm-9">
                                <textarea name="summary" id="summary" rows=""5 required cols="30" rows="10"
                                          class="form-control form-control-sm"> {{ old('summary') }} </textarea>
                                @error('summary')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Description:</label>
                            <div class="col-sm-9">
                                <textarea name="description" id="description" rows=""5 required cols="30" rows="10"
                                          class="form-control form-control-sm">{{ old('description') }}</textarea>
                                @error('description')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Category:</label>
                            <div class="col-sm-9">
                                <select name="category_id" id="category_id" class="form-control form-control-sm" required>
                                    <option value="1">News</option>
                                    <option value="2">Politics</option>
                                    <option value="3">International</option>
                                    <option value="4">Blog</option>
                                </select>
                                @error('category_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Status:</label>
                            <div class="col-sm-9">
                                <select name="status" id="status" class="form-control form-control-sm" required>
                                    <option value="active">Published</option>
                                    <option value="inactive">Un-published</option>
                                </select>
                                @error('status')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="cos-sm-3">Image:</label>
                            <div class="col-sm-9">
                                <input type="file" name="image" accept="image/*">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                           <div class="offset-sm-3 col-sm-9">
                               <button class="btn btn-danger btn-sm" type="reset">Reset</button>
                               <button class="btn btn-danger btn-sm" type="submit">Submit</button>
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
