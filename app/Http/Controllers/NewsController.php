<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Psy\Util\Str;
use File;

class NewsController extends Controller
{
    public function __construct()
    {
      //  $this->middleware('auth');
    }
    public function index()
    {

        $news_list = array(
            array('sn'        =>  1,
                'title'     =>  "First title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Inactive",
                'category'  =>  "News",
                'image'     =>  "1.jpg",
            ),
            array('sn'        =>  2,
                'title'     =>  "Second title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Inactive",
                'category'  =>  "News",
                'image'     =>  "2.jpg",
            ),
            array('sn'        =>  3,
                'title'     =>  "Third title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Active",
                'category'  =>  "News",
                'image'     =>  "3.jpg",
            )
        );
        return view('blog/blog-list')->with('data', $news_list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // fetch data from request
        //$request->request->add(['is_featured' => 1]); //this will set the data in $request thought this data is not send from form.
        //$data = $request->except(['_token']); // all data except specified
        //$data = $request->only(['title', 'summary']); // all data except specified

        //dd($request->all());
        $request->request->add(['is_featured' => 1]);
        $rules = array(
            'title'      => 'required|string|max:150|min:5',
            'summary'    => ['required','string'],
            'description'=> 'nullable|string',
            //'category_id'=> 'required|exists:categories,id', // foreign key validation
            'category_id'=> 'required|in:1,2,3,4',
            'status'=> 'required|in:active,inactive',
            'image' => 'sometimes|image|max:5000'    // jpg, ng, gif, bmp, svg, webp
        );
        $request->validate($rules);

        $data = $request->except('image');
        if($request->image){
            $name = "image-".date("YmdHis").rand(0,999).'.'.$request->image->getClientOriginalExtension();
            $path = public_path()."/uploads/image";
            if(File::exists($path)){
                File::makeDirectory($path);
            }
            $success = $request->image->move($path, $name);
            if($success){
                $data['image'] = $name;
                // thumbnail generate
            }


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
