<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function getBlog()
    {
        $user_list = array(
            array('sn'        =>  1,
                'title'     =>  "First title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Inactive",
                'category'  =>  "News",
                'image'     =>  "1.jpg",
            ),
            array('sn'        =>  2,
                'title'     =>  "Second title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Inactive",
                'category'  =>  "News",
                'image'     =>  "2.jpg",
            ),
            array('sn'        =>  3,
                'title'     =>  "Third title",
                'summary'   =>  "Lorem ipsum dolor ist amet, consectetur adipisicing",
                'status'    =>  "Active",
                'category'  =>  "News",
                'image'     =>  "3.jpg",
            )
        );
        return view('blog/blog-list')->with('data', $user_list);
    }

    public function createForm()
    {
        return view('blog.form');

    }
}
