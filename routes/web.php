<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => "App\Http\Controllers"], function (){
    //Route::resource("news", "NewsController");
    Route::get('/news/create', "NewsController@create")->name('news.create');
    Route::post('/news', "NewsController@store")->name('news.store');
    Route::get('/news', "NewsController@index")->name('news.index');
    Route::get('/news/{id}', "BLogController@show")->name('news.show');
    Route::get('/news/{id}/edit', "BLogController@show")->name('news.edit');
    Route::match(['put', 'patch'], '/news/{id}', "BLogController@update")->name('news.update');
    Route::delete('/news/{id}', "BLogController@destroy")->name('news.destroy');
});

Route::group(['namespace'=>"App\Http\Controllers"], function(){
   Route::get('/blog', "BlogController@getBlog")->name('blog-list');
   Route::get('/blog/create', "BlogController@createForm")->name('blog-create');
});

Route::get("/blog/{slug}", function (){
    echo "This is for blog details";
})->name("blog-detail");

//Auth::routes(['register' =>true]);



/*Auth::routes();

Route::group(['prefix' => 'admin', 'namespace' => "App\Http\Controllers"], function(){
    Route::get('/', 'HomeController@admin')->name('admin');
});

Route::group(['prefix' => 'user', 'namespace' => "App\Http\Controllers"], function(){
    Route::get('/', 'HomeController@user')->name('user');
});*/

